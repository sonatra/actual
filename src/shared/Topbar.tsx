import { AppBar, Toolbar } from "@material-ui/core";
import React, { FC } from "react";
import { ButtonLink } from "./ButtonLink";

export interface TopbarProps {}
export const Topbar: FC<TopbarProps> = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <ButtonLink linkProps={{ to: "/" }} color="inherit">
          Accueil
        </ButtonLink>
        <ButtonLink linkProps={{ to: "/informations" }} color="inherit">
          Informations
        </ButtonLink>
      </Toolbar>
    </AppBar>
  );
};
