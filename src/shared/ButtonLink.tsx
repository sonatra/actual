import { Button, ButtonProps } from "@material-ui/core";
import React, { FC } from "react";
import { Link, LinkProps } from "react-router-dom";

export interface ButtonLinkProps extends ButtonProps {
  linkProps: LinkProps;
}

export const ButtonLink: FC<ButtonLinkProps> = ({ linkProps, ...props }) => {
  return (
    <Link
      style={{ textDecoration: "none", color: "inherit", ...props.style }}
      {...linkProps}
    >
      <Button {...props} />
    </Link>
  );
};
