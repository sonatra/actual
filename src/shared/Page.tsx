import { Box, BoxProps } from "@material-ui/core";
import { FC } from "react";

export interface PageProps extends BoxProps {}

export const Page: FC<PageProps> = ({ children, ...props }) => (
  <Box p={1} {...props}>
    {children}
  </Box>
);
