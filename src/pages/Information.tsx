import { yupResolver } from "@hookform/resolvers/yup";
import { Box, CircularProgress, debounce, Typography } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import { format } from "date-fns";
import locale from "date-fns/locale/fr";
import React, { FC, useCallback, useMemo } from "react";
import { useForm } from "react-hook-form";
import { date, object, string } from "yup";
import { User } from "../entities/user";
import { Page } from "../shared/Page";
import { useAppStore, UserCreateDTO } from "../stores/app";
import { getRemainingDaysSentence } from "../utils/date";
import { Status } from "../utils/type";

export const UserSchema = () => {
  const Today = new Date();
  Today.setHours(0, 0, 0, 0);
  return object().shape({
    firstname: string(),
    birthdate: date().min(
      Today,
      "La date ne peut être antérieur d'aujourd'hui"
    ),
  });
};

export interface InformationsProps {}
export const Informations: FC<InformationsProps> = () => {
  const { user, create, createStatus, update, updateStatus } = useAppStore();
  const { handleSubmit, register, errors, watch } = useForm<User>({
    defaultValues: {
      firstname: user?.firstname || "",
      birthdate: format(
        user ? new Date(user.birthdate) : new Date(),
        "yyyy-MM-dd",
        { locale }
      ),
    },
    shouldFocusError: false,
    resolver: yupResolver(UserSchema()),
  });

  const birthdate = watch("birthdate");

  const loading = useMemo(
    () => createStatus === Status.PENDING || updateStatus === Status.PENDING,
    [createStatus, updateStatus]
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onSubmit = useCallback(
    debounce((values: UserCreateDTO) => {
      user?.id ? update(user.id, values) : create(values);
    }, 500),
    [user]
  );

  return (
    <Page>
      <Typography variant="h5" paragraph>
        Informations
      </Typography>
      <Box>
        <form onChange={handleSubmit(onSubmit)}>
          Bonjour{" "}
          <InputBase
            placeholder="*prénom"
            inputRef={register}
            name="firstname"
            style={{ width: 120 }}
          />
          votre anniversaire{" "}
          {!errors.birthdate
            ? `est ${getRemainingDaysSentence(new Date(birthdate))}`
            : "ne peut être inférieur à la date d'aujourd'hui"}{" "}
          <InputBase
            type="date"
            placeholder="votre date de naissance"
            inputRef={register}
            style={{ width: 50 }}
            name="birthdate"
          />
          <Box display="inline-block" ml={1}>
            {loading && <CircularProgress size={10} thickness={12} />}
          </Box>
        </form>
      </Box>
    </Page>
  );
};
