import { Typography } from "@material-ui/core";
import React, { FC } from "react";
import { Link } from "react-router-dom";
import { Page } from "../shared/Page";
import { useAppStore } from "../stores/app";
import { getRemainingDaysSentence } from "../utils/date";

export interface HomeProps {}

export const Home: FC<HomeProps> = () => {
  const user = useAppStore((state) => state.user);
  return (
    <Page>
      <Typography variant="h5" paragraph>
        Accueil
      </Typography>
      {user ? (
        <Typography variant="subtitle1">
          Bonjour {user.firstname} votre anniversaire est{" "}
          {getRemainingDaysSentence(new Date(user.birthdate))}
        </Typography>
      ) : (
        <Typography variant="subtitle1">
          Bonjour, veuillez renseigner vos informations en suivant ce lien :{" "}
          <Link to="/informations">informations</Link>
        </Typography>
      )}
    </Page>
  );
};
