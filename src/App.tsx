import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import { Home } from "./pages/Home";
import { Informations } from "./pages/Information";
import { Topbar } from "./shared/Topbar";

function App() {
  return (
    <div>
      <Router>
        <Topbar />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/informations">
            <Informations />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
