// Let api to be overrided by env
export const API = process.env.REACT_APP_API || "https://reqres.in/api";
