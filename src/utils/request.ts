import axios, { AxiosRequestConfig, Method } from "axios";
import { SetState, State } from "zustand";
import { API } from "./constant";
import { Status } from "./type";

export interface RequestProps<T> extends AxiosRequestConfig {
  method: Method;
  status: keyof T;
}

export const Request = <T extends State, U>(
  set: SetState<T>,
  url: string
) => async (
  { status, ...axiosProps }: RequestProps<T>,
  onSuccess: (data: U) => void,
  onFailed: (error: any) => void
) => {
  if (status === Status.PENDING) return;
  try {
    set((state) => ({ ...state, [status]: Status.PENDING }));
    const { data: result } = await axios.create({ baseURL: API.concat(url) })(
      axiosProps
    );
    set((state) => ({ ...state, [status]: Status.SUCCESS }));
    onSuccess(result);
  } catch (error) {
    set((state) => ({ ...state, [status]: Status.FAILED }));
    onFailed(error);
  }
};
