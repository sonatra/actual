import { differenceInCalendarDays, isToday } from "date-fns";

export const getRemainingDaysSentence = (date: Date) =>
  isToday(new Date(date))
    ? "aujourd'hui"
    : `dans ${differenceInCalendarDays(new Date(date), new Date())} jours`;
