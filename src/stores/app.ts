import create from "zustand";
import axios from "axios";
import { API } from "../utils/constant";
import { Request } from "../utils/request";
import { User } from "../entities/user";
import { Status } from "../utils/type";

export type UserCreateDTO = Omit<User, "id">;
export type UserUpdateDTO = Partial<Omit<User, "id">>;

export type AppStore = {
  user?: User;
  createStatus: Status;
  updateStatus: Status;
  errors?: string;
  create: (userDTO: UserCreateDTO) => void;
  update: (id: string, userDTO: UserUpdateDTO) => void;
  create2: (userDTO: UserCreateDTO) => void;
  update2: (id: string, userDTO: UserUpdateDTO) => void;
};

export const useAppStore = create<AppStore>((set, get) => {
  const userRequest = Request<AppStore, User>(set, "/users?delay=2");
  return {
    user: undefined,
    createStatus: Status.NULL,
    updateStatus: Status.NULL,
    create: async (dto) => {
      if (get().createStatus === Status.PENDING) return;
      try {
        set({ createStatus: Status.PENDING });
        const { data: user } = await axios.post(`${API}/users?delay=2`, dto);
        set({ user });
        set({ createStatus: Status.SUCCESS });
      } catch (error) {
        set({ createStatus: Status.FAILED });
        set({ errors: "La création de l'utilisateur a échoué" });
      }
    },
    update: async (id, dto) => {
      if (get().updateStatus === Status.PENDING) return;
      try {
        set({ updateStatus: Status.PENDING });
        const { data: user } = await axios.patch(
          `${API}/users/${id}?delay=2`,
          dto
        );
        set((state) => ({ user: Object.assign({}, state.user, user) }));
        set({ updateStatus: Status.SUCCESS });
      } catch (error) {
        set({ updateStatus: Status.FAILED });
        set({ errors: "La mise à jour de l'utilisateur a échoué" });
      }
    },
    create2: (dto) =>
      userRequest(
        { status: "createStatus", method: "POST", data: dto },
        (data) => set({ user: data }),
        () => set({ errors: "La création de l'utilisateur a échoué" })
      ),
    update2: (id, dto) =>
      userRequest(
        { status: "updateStatus", method: "PATCH", data: dto, params: { id } },
        (data) =>
          set((state) => ({ user: Object.assign({}, state.user, data) })),

        () => set({ errors: "La mise à jour de l'utilisateur a échoué" })
      ),
  };
});
