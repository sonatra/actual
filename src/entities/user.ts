export interface User {
  id: string;
  firstname: string;
  birthdate: string;
}
